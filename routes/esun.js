"use strict";

const express = require('express');
const router = express.Router();
const request = require('request');
const es = require('../esun-first');

const default_qs = { client_id: es.client_id };
function esunapi(suffix, callback) {
	const options = {
		url: `${es.prefix}${suffix}`,
		qs: { client_id: es.client_id }
	};
	request(options, callback);
}

router.get('/balance_out', function(req, res) {
	esunapi(`customers/${es.account[0].username}/accounts/${es.account[0].account}/accounts_balance`, (e,r,b)=>
		res.json(JSON.parse(b))
	);
});

router.get('/balance_in', function(req, res) {
	esunapi(`customers/${es.account[1].username}/accounts/${es.account[1].account}/accounts_balance`, (e,r,b)=>
		res.json(JSON.parse(b))
	);
});

router.get('/demo', function(req, res) {
	res.render('demo');
});

module.exports = router;
