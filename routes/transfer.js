const request = require('request');

const es = require('../esun-first');

module.exports = function(amount) {
	const auth_options = {
		url: `${es.prefix}customers/oauth/authorize`,
		headers: {
			Authorization: 'Basic ' + (new Buffer(`${es.client_id}:${es.client_secret}`)).toString('base64'),
		},
		form: {
			grant_type: 'password',
			username: es.account[0].username,
			password: es.account[0].password,
			scope: '/customers'
		}
	};
	function transfer(err, res, body) {
		console.log('first request', body);
		body = JSON.parse(body);
		const trans_options = {
			url: `${es.prefix}customers/outbank_transfer`,
			qs: { client_id: es.client_id },
			headers: {
				authorization: `Bearer ${body.access_token}`,
			},
			json: true,
			body: {
				outward_account: es.account[0].account,
				inward_account: es.account[1].account,
				transfer_amount: amount,
				id: es.account[0].username
			}
		};
		console.log(trans_options);
		request.post(trans_options, (err, res, body) => console.log('second request', body));
	}
	request.post(auth_options, transfer);
};