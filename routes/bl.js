"use strict";

const express = require('express');
const router = express.Router();
const request = require('request');
const bl = require('../bl-config');

router.post('/translate', function(req, res) {
	const options = {
		url: 'https://gateway.watsonplatform.net/language-translation/api/v2/translate',
		headers: {
			accept: 'application/json'
		},
		form: {
			text: req.body.text,
			source: "en",
			target: "es"
		}
	};
	request.post(options, function(e, r, b) {
		res.json(JSON.parse(b));
	}).auth(bl.username, bl.password, false);
});

module.exports = router;
