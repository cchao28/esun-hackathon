"use strict";

const express = require('express');
const router = express.Router();
const db = require('../models/db');
const marked = require('marked');
const passport = require('passport');
const logger = require('./logger');
const transfer = require('./transfer');

marked.setOptions({
	renderer: new marked.Renderer(),
	gfm: true,
	tables: true,
	breaks: false,
	pedantic: false,
	sanitize: true,
	smartLists: true,
	smartypants: false
});

router.get('/', function(req, res) {
	res.render('index', {user: req.user});
});

router.get('/signup', function(req, res) {
	res.render('signup', {user: req.user});
});

router.post('/signup', function(req, res) {
	const x = new db.User({
		username: req.body.username,
		account: req.body.account
	});

	db.User.register(x, req.body.password, function(err) {
		if(err) {
			console.error('error while user register!', err);
			res.redirect('/');
		}
		console.log('user registered!');
		res.redirect('/');
	});
});

router.get('/login', function(req, res) {
	res.render('login', {user: req.user});
});

router.get('/debug', function(req, res) {
	db.User.find({}, function(err, data) {
		if(!err) {
			console.log(data);
			res.render('debug', {data: data});
		}
		else {
			console.error(err);
			res.render('debug', {data: {}});
		}
	});
});


router.post('/login', passport.authenticate('local'), function(req, res) {
	res.redirect('/');
});

router.get('/logout', function(req, res) {
	req.logout();
	res.redirect('/');
});

router.get('/account', function(req, res) {
	if(!req.user) {
		res.redirect('/');
	}
	else {
		res.render('account', {user: req.user});
	}
});

router.get('/activity', function(req, res) {
	db.Activity.find({username: req.user.username}, function(err, data) {
		if(err) {
			res.json({});
		}
		else {
			res.json(data);
		}
	});
});

router.get('/profile/:id', function(req, res) {
	db.User.findOne({username: req.params.id}, function(err, data) {
		if(err) {
			res.render('log', { message: 'profile not found' });
		}
		else {
			res.render('profile', { data: data, user: req.user });
		}
	});
});

router.get('/submit/contest', function(req, res) {
	if(!req.user) {
		return res.redirect('/login');
	}
	res.render('submit/contest', {user: req.user});
});

router.get('/submit/project', function(req, res) {
	if(!req.user) {
		return res.redirect('/login');
	}
	res.render('submit/project', {user: req.user});
});

router.get('/browse/contest', function(req, res) {
	db.Contest.find({}, function(err, data) {
		if(!err) {
			res.render('browse/contest', {data: data, user: req.user});
		}
		else {
			console.error(err);
			res.render('browse/contest', {data: {}, user: req.user});
		}
	});
});

router.get('/browse/project', function(req, res) {
	db.Project.find({}, function(err, data) {
		if(!err) {
			res.render('browse/project', {data: data, user: req.user});
		}
		else {
			console.error(err);
			res.render('browse/project', {data: {}, user: req.user});
		}
	});
});

router.post('/submit/contest', function(req, res) {
	const x = new db.Contest({
		title: req.body.title,
		endDate: new Date(req.body.endDate),
		description: req.body.description,
		prizes: [req.body.firstPrize, req.body.secondPrize, req.body.thirdPrize],
		judge: req.body.judge,
		author: req.user.username,
		status: 0
	});
	logger(req.user.username, new Date(), '發表競賽 - ' + x.title, -x.prizes[0]-x.prizes[1]-x.prizes[2]);
	x.save(function(err) {
		if(err) {
			console.error(err);
		}
		else {
			console.log('data saved!');
		}
		res.redirect('/browse/contest');
	});
});

router.post('/submit/project', function(req, res) {
	const x = new db.Project({
		title: req.body.title,
		endDate: new Date(req.body.endDate),
		description: req.body.description,
		budget: req.body.budget,
		author: req.user.username,
		status: 0
	});
	logger(req.user.username, new Date(), '發表專案 - ' + x.title, -req.body.budget);
	x.save(function(err) {
		if(err) {
			console.error(err);
		}
		else {
			console.log('data saved!');
		}
		res.redirect('/browse/project');
	});
});


router.get('/view/contest/:id', function(req, res) {
	db.Contest.findById(req.params.id, function(err, data) {
		if(err) {
			res.render('log', { message: 'contest not found' });
		}
		else {
			// data.description = marked(data.description);
			res.render('view/contest', { data: data, user: req.user });
		}
	});
});


router.get('/view/project/:id', function(req, res) {
	db.Project.findById(req.params.id, function(err, data) {
		if(err) {
			res.render('log', { message: 'project not found' });
		}
		else {
			// data.description = marked(data.description);
			res.render('view/project', { data: data, user: req.user });
		}
	});
});

router.get('/reply/contest/:id', function(req, res) {
	if(!req.user) {
		return res.redirect('/login');
	}
	db.Contest.findById(req.params.id, function(err, data) {
		if(err) {
			res.render('log', { message: 'contest not found' });
		}
		else {
			res.render('reply/contest', {data : data, user: req.user});
		}
	});
});

router.get('/reply/project/:id', function(req, res) {
	if(!req.user) {
		return res.redirect('/login');
	}
	db.Project.findById(req.params.id, function(err, data) {
		if(err) {
			res.render('log', { message: 'project not found' });
		}
		else {
			res.render('reply/project', {data : data, user: req.user});
		}
	});
});

router.post('/reply/contest/:id', function(req, res) {
	const x = new db.ContestReply({
		author: req.user.username,
		message: req.body.message,
		id: req.params.id
	});
	db.Contest.findById(req.params.id, function(err, x) {
		x.status++;
		x.save();
	});
	x.save(function(err) {
		if(err) {
			console.error(err);
		}
		else {
			console.log('data saved!');
		}
		res.redirect('/');
	});
});

router.post('/reply/project/:id', function(req, res) {
	const x = new db.ProjectReply({
		author: req.user.username,
		message: req.body.message,
		id: req.params.id
	});
	db.Project.findById(req.params.id, function(err, x) {
		x.status++;
		x.save();
	});
	x.save(function(err) {
		if(err) {
			console.error(err);
		}
		else {
			console.log('data saved!');
		}
		res.redirect('/');
	});
});

router.get('/manage/contest/:id', function(req, res) {
	// TODO: verify user
	db.ContestReply.find({id: req.params.id}, function(err, data) {
		if(err) {
			res.render('log', {message: 'not found'});
		}
		else {
			data = data.map(function(x) {
				x.message = marked(x.message);
				return x;
			});
			res.render('manage/contest', {data: data, user: req.user, id: req.params.id});
		}
	});
});

router.get('/manage/project/:id', function(req, res) {
	// TODO: verify user
	db.ProjectReply.find({id: req.params.id}, function(err, data) {
		if(err) {
			res.render('log', {message: 'not found'});
		}
		else {
			res.render('manage/project', {data: data, user: req.user, id: req.params.id});
		}
	});
});

router.post('/manage/contest/:id', function(req, res) {
	// TODO: verify user
	db.Contest.findById(req.params.id, function(err, data) {
		if(err) {
			res.render('log', {message: 'not found'});
			res.redirect('/');
		}
		else {
			data.status = -1;
			data.save();
			let arr = [];
			for(var prop in req.body) {
				console.log(prop, req.body[prop]);
				arr.push([prop, req.body[prop]]);
			}
			arr.sort((a, b) => +b[1]-a[1]);
			arr.splice(3);
			for(var i = 0; i < arr.length; ++i) {
				if(+data.prizes[i] === 0) continue;
				logger(arr[i][0], new Date(), `${data.title} - 贏得第${i+1}名`, data.prizes[i]);
				transfer(+data.prizes[i]);
			}
			res.redirect('/');
		}
	});
});

module.exports = router;