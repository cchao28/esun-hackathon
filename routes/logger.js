"use strict";

const db = require('../models/db');

module.exports = function(user, date, act, balance) {
	db.User.findOne({username: user}, function(err, data) {
		if(err) {
			console.error(err);
		}
		else {
			const x = new db.Activity({
				username: user,
				date: new Date(date),
				description: act,
				balance: balance
			});
			x.save();
		}
	});
};