"use strict";

const express = require('express');
const path = require('path');
const sassMiddleware = require('node-sass-middleware');
const jade = require('jade');
const mongoose = require('mongoose');
const db = require('./models/db');
const config = require('./config');
const routes = require('./routes/index');
const esunapi = require('./routes/esun');
const blapi = require('./routes/bl');
const bodyParser = require('body-parser');
const cfenv = require('cfenv');
const mongo_express = require('mongo-express/lib/middleware');
const mongo_express_config = require('./mongo-config');
const appEnv = cfenv.getAppEnv();

// for member
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');

const app = express();

// views middleware
app.set('view engine', 'jade');
app.use('/css', sassMiddleware({
	src: path.join(__dirname, 'views/css'),
	dest: path.join(__dirname, 'public/css'),
	debug: config.debug,
	outputStyle: 'extended'
}));

app.use(express.static(path.join(__dirname, 'public')));

// support POST method
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(require('express-session')({
	secret: 'keyboard cat',
	resave: false,
	saveUninitialized: false
}));

// passport config
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(db.User.authenticate()));
passport.serializeUser(db.User.serializeUser());
passport.deserializeUser(db.User.deserializeUser());

app.use('/mongo_express', mongo_express(mongo_express_config));

app.use('/', routes);
app.use('/', esunapi);
app.use('/', blapi);

app.listen(appEnv.port, '0.0.0.0', () =>
	console.log(`Web server listening on port ${appEnv.url}`)
);

