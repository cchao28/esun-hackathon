'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const config = require('../config');
const passportLocalMongoose = require('passport-local-mongoose');

const db = process.env.LOCALDB ? config.localdb : config.mongodb;

mongoose.connect(db);

console.log('connect to', db);

const userSchema = new Schema({
	username: {
		type: String,
		required: true,
	},
	password: {
		type: String
	},
	account: {
		type: String,
		required: true,
	},
	balance: {
		type: Number,
		default: 0
	}
});
userSchema.plugin(passportLocalMongoose);

const contestSchema = new Schema({
	title: {
		type: String,
		required: true,
		default: '未命名競賽'
	},
	endDate: {
		type: Date,
		require: true,
		default: Date.now
	},
	description: {
		type: String,
		require: true
	},
	prizes: {
		type: [Number],
		require: true
	},
	author: {
		type: String,
		require: true,
		default: 'Anonymous'
	},
	judge: {
		type: String,
		require: true
	},
	status: {
		type: Number,
		default: 0
	}
});

const projectSchema = new Schema({
	title: {
		type: String,
		required: true,
		default: '未命名專案'
	},
	endDate: {
		type: Date,
		require: true,
		default: Date.now
	},
	description: {
		type: String
	},
	budget: {
		type: Number,
		require: true
	},
	author: {
		type: String,
		require: true,
		default: 'Anonymous'
	},
	status: {
		type: Number,
		default: 0
	}
});

const contestReplySchema = new Schema({
	author: String,
	message: String,
	id: Schema.Types.ObjectId
});

const projectReplySchema = new Schema({
	author: String,
	message: String,
	id: Schema.Types.ObjectId
});

const activitySchema = new Schema({
	username: String,
	date: Date,
	description: String,
	balance: Number
});

module.exports = {
	User: mongoose.model('user', userSchema),
	Contest: mongoose.model('contest', contestSchema),
	Project: mongoose.model('project', projectSchema),
	ContestReply: mongoose.model('contest_reply', contestReplySchema),
	ProjectReply: mongoose.model('project_reply', projectReplySchema),
	Activity: mongoose.model('activity', activitySchema)
};
