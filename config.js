'use strict';

module.exports = {
	port: process.env.PORT || 3000,
	localdb: 'mongodb://localhost/esun',
	mongodb: 'mongodb://esun:esun@ds017852.mlab.com:17852/esun',
	debug: true
};
